# SessionTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prepaid_invoice** | **bool** |  | [optional] 
**manual_review** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

