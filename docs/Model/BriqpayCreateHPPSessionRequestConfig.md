# BriqpayCreateHPPSessionRequestConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**showcart** | **bool** | Should the cart be displayed to the consumer? | 
**logo_url** | **string** | Your store logo to be used on the Hosted Page | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

