# SessionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**\Briqpay\Models\Currency**](Currency.md) |  | 
**locale** | [**\Briqpay\Models\Locale**](Locale.md) |  | 
**country** | [**\Briqpay\Models\Country**](Country.md) |  | 
**amount** | **int** | The total amount of the order including VAT | 
**recurring** | [**\Briqpay\Models\SessionRequestRecurring**](SessionRequestRecurring.md) |  | [optional] 
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  | 
**merchanturls** | [**\Briqpay\Models\MerchantUrls**](MerchantUrls.md) |  | 
**merchantconfig** | [**\Briqpay\Models\MerchantConfig**](MerchantConfig.md) |  | [optional] 
**reference** | [**\Briqpay\Models\MerchantReference**](MerchantReference.md) |  | [optional] 
**orgnr** | **string** |  | [optional] 
**billingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional] 
**shippingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional] 
**merchant_billing** | [**\Briqpay\Models\SessionRequestMerchantBilling**](SessionRequestMerchantBilling.md) |  | [optional] 
**merchant_shipping** | [**\Briqpay\Models\SessionRequestMerchantShipping**](SessionRequestMerchantShipping.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

