# SessionPurchasepaymentmethod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservationid** | **string** |  | [optional] 
**sessionid** | **string** |  | [optional] 
**pspid** | **string** |  | [optional] 
**pspname** | **string** | The internal PSPName used | [optional] 
**name** | **string** | The customer-facing PSPname | [optional] 
**autocapture** | **bool** | Was this order alread captured? | [optional] 
**invoicemarkdown** | **string** | Additional data added to the invoice markdown field | [optional] 
**reference** | **string** | Additional reference added to the invoice | [optional] 
**email** | **string** | Customer added an specific email to recieve the invoice | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

