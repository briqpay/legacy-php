# UpdateSessionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**\Briqpay\Models\Currency**](Currency.md) |  | 
**locale** | [**\Briqpay\Models\Locale**](Locale.md) |  | 
**country** | [**\Briqpay\Models\Country**](Country.md) |  | 
**sessionid** | **string** | The sessionid to be patched | 
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  | 
**merchanturls** | [**\Briqpay\Models\MerchantUrls**](MerchantUrls.md) |  | 
**amount** | **int** | The total amount of the order including VAT | 
**reference** | [**\Briqpay\Models\MerchantReference**](MerchantReference.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

