# ReadSignupResponseMerchanturls

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**terms** | **string** |  | [optional] 
**signupwebhook** | **string** | The url where briqpay should post data for you to validate the registration | [optional] 
**redirecturl** | **string** | The page where briqpay should redirect the customer after a successful registration | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

