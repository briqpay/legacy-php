# CartItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**producttype** | [**\Briqpay\Models\ProductType**](ProductType.md) |  | [optional] 
**reference** | [**\Briqpay\Models\ProductReference**](ProductReference.md) |  | [optional] 
**name** | **string** |  | [optional] 
**quantity** | **int** |  | [optional] 
**quantityunit** | **string** |  | [optional] 
**unitprice** | **int** | Unit price exckluding VAT | [optional] 
**taxrate** | [**\Briqpay\Models\TaxRate**](TaxRate.md) |  | [optional] 
**discount** | **int** | Discount value in percentages. 10% &#x3D; 1000 | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

