# OrderNoteConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_inputs** | [**\Briqpay\Models\CustomInput[]**](CustomInput.md) | Avaliable custom inputs fields that should be collected in the order_note section of the checkout | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

