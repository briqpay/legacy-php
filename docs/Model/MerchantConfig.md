# MerchantConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxamount** | **bool** | Should the max amount rules be applied to this purchase? | [optional] 
**creditscoring** | **bool** | Should the creditscoring rules be applied to this purchase? | [optional] 
**productid** | **string** | The product ID identifier to  use for this purchase, only use if you have multiple setups with Briqpay | [optional] [default to '']
**order_note** | [**\Briqpay\Models\OrderNoteConfiguration**](OrderNoteConfiguration.md) |  | [optional] 
**payment** | [**\Briqpay\Models\PaymentConfiguration**](PaymentConfiguration.md) |  | [optional] 
**billing** | [**\Briqpay\Models\BillingConfiguration**](BillingConfiguration.md) |  | [optional] 
**shipping** | [**\Briqpay\Models\ShippingConfiguration**](ShippingConfiguration.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

