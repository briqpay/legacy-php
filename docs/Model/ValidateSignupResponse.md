# ValidateSignupResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **bool** | true/false depending on the validation result | 
**errors** | [**\Briqpay\Models\ValidateSignupResponseError[]**](ValidateSignupResponseError.md) | Errors to display to the user as to why the registration failed | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

