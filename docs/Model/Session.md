# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**\Briqpay\Models\Currency**](Currency.md) |  | [optional] 
**locale** | [**\Briqpay\Models\Locale**](Locale.md) |  | [optional] 
**country** | [**\Briqpay\Models\Country**](Country.md) |  | [optional] 
**orgnr** | **string** |  | [optional] 
**billingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional] 
**shippingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional] 
**reference** | [**\Briqpay\Models\MerchantReference**](MerchantReference.md) |  | [optional] 
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  | [optional] 
**merchanturls** | [**\Briqpay\Models\MerchantUrls**](MerchantUrls.md) |  | [optional] 
**amount** | **int** | The total amount of the order including VAT | [optional] 
**snippet** | **string** |  | [optional] 
**tags** | [**\Briqpay\Models\SessionTags**](SessionTags.md) |  | [optional] 
**sessionid** | **string** | The unique identifier for this specific session | [optional] 
**state** | [**\Briqpay\Models\SessionState**](SessionState.md) |  | [optional] 
**createddate** | [**\DateTime**](\DateTime.md) |  | [optional] 
**ordernote** | [**\Briqpay\Models\OrderNote**](OrderNote.md) |  | [optional] 
**purchasepaymentmethod** | [**\Briqpay\Models\SessionPurchasepaymentmethod**](SessionPurchasepaymentmethod.md) |  | [optional] 
**extra_company_fields** | [**\Briqpay\Models\ExtraCompanyFields**](ExtraCompanyFields.md) |  | [optional] 
**rulesresult** | [**\Briqpay\Models\PSPRuleResults[]**](PSPRuleResults.md) | Get detailed information on what rules triggered for this purchase | [optional] 
**token** | **string** | The Bearer token to use for all subsequent calls on this session - This is only return on Session Create Calls | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

