# BriqpayCreateHPPSessionRequestDeliverymethod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Can be value of &#x27;email&#x27;, &#x27;link&#x27; or &#x27;sms&#x27; | 
**destination** | **string** | The destination of the hostedpage should be email-address or cellphone number | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

