# CreateCaptureRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionid** | **string** |  | [optional] 
**amount** | **int** | The total amount of the capture including VAT | [optional] 
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) | The array of cart items to be captured | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

