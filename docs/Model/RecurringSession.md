# RecurringSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionid** | **string** | The ID of the session | [optional] 
**amount** | **int** | The order amount | [optional] 
**amountexvat** | **int** | the amount excluding VAT | [optional] 
**date** | **string** | The date of the session | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

