# CancelRecurringTokenRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurring_token** | **string** | The recurring token you wish to cancel | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

