# BriqpayCreateHPPSessionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checkoutsessionid** | **string** | The sessionId that you recieved after creating a checkout session | 
**deliverymethod** | [**\Briqpay\Models\BriqpayCreateHPPSessionRequestDeliverymethod**](BriqpayCreateHPPSessionRequestDeliverymethod.md) |  | 
**config** | [**\Briqpay\Models\BriqpayCreateHPPSessionRequestConfig**](BriqpayCreateHPPSessionRequestConfig.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

