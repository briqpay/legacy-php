# PatchSessionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionid** | **string** | The sessionid to be patched | 
**merchanturls** | [**\Briqpay\Models\MerchantUrls**](MerchantUrls.md) |  | 
**reference** | [**\Briqpay\Models\MerchantReference**](MerchantReference.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

