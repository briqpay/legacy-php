# SessionRequestRecurring

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** | Should this purchase create a recurring token for subsequent subscription charges? | [optional] 
**planid** | **string** | the unique planid to be used in this subscription | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

