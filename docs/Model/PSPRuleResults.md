# PSPRuleResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pspname** | **string** | What PSP was triggered | [optional] 
**rules_result** | [**\Briqpay\Models\IndividualRuleResult[]**](IndividualRuleResult.md) | All rules that failed for this method | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

