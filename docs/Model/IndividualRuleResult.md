# IndividualRuleResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outcome** | **bool** |  | [optional] 
**friendlyname** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

