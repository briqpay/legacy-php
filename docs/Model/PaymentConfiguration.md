# PaymentConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**psp_rules_override** | [**\Briqpay\Models\PaymentConfigurationPspRulesOverride**](PaymentConfigurationPspRulesOverride.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

