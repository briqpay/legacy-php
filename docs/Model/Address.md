# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyname** | **string** |  | [optional] 
**firstname** | **string** |  | [optional] 
**lastname** | **string** |  | [optional] 
**streetaddress** | **string** |  | [optional] 
**streetaddress2** | **string** |  | [optional] 
**zip** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**cellno** | **string** |  | [optional] 
**email** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

