# Briqpay\OrderManagementApi

All URIs are relative to *https://playground-api.briqpay.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**captureOrder**](OrderManagementApi.md#captureorder) | **POST** /order-management/v1/capture-order | Capture a placed order
[**refundOrder**](OrderManagementApi.md#refundorder) | **POST** /order-management/v1/refund-order | Refunding a captured

# **captureOrder**
> \Briqpay\Models\CaptureResponse captureOrder($body)

Capture a placed order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\OrderManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\CreateCaptureRequest(); // \Briqpay\Models\CreateCaptureRequest | Capture order

try {
    $result = $apiInstance->captureOrder($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->captureOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\CreateCaptureRequest**](../Model/CreateCaptureRequest.md)| Capture order | [optional]

### Return type

[**\Briqpay\Models\CaptureResponse**](../Model/CaptureResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refundOrder**
> \Briqpay\Models\RefundResponse refundOrder($body)

Refunding a captured

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\OrderManagementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\CreateRefundRequest(); // \Briqpay\Models\CreateRefundRequest | Refund money from captured order

try {
    $result = $apiInstance->refundOrder($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderManagementApi->refundOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\CreateRefundRequest**](../Model/CreateRefundRequest.md)| Refund money from captured order | [optional]

### Return type

[**\Briqpay\Models\RefundResponse**](../Model/RefundResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

