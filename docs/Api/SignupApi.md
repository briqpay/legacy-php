# Briqpay\SignupApi

All URIs are relative to *https://playground-api.briqpay.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSignup**](SignupApi.md#createsignup) | **POST** /signup/v1/session | Create a new signup session
[**readSignup**](SignupApi.md#readsignup) | **GET** /signup/v1/session | Read an ongoing signup session
[**sendValidateResponse**](SignupApi.md#sendvalidateresponse) | **POST** /signup/v1/hooks/validated | Validation Result
[**yoursignupwebhookPost**](SignupApi.md#yoursignupwebhookpost) | **POST** /yoursignupwebhook | SignupWebhook Request

# **createSignup**
> \Briqpay\Models\CreateSignupResponse createSignup($body)

Create a new signup session

First step in the signup process is to generate a signup session. The response will provide you with a snippet that you should render in your frontend ![Briqpay Signup Sequence](https://cdn.briqpay.com/static/developer-portal/signup-short-integration.png)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\SignupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\CreateSignupRequest(); // \Briqpay\Models\CreateSignupRequest | 

try {
    $result = $apiInstance->createSignup($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SignupApi->createSignup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\CreateSignupRequest**](../Model/CreateSignupRequest.md)|  | [optional]

### Return type

[**\Briqpay\Models\CreateSignupResponse**](../Model/CreateSignupResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **readSignup**
> \Briqpay\Models\ReadSignupResponse readSignup()

Read an ongoing signup session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\SignupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->readSignup();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SignupApi->readSignup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Briqpay\Models\ReadSignupResponse**](../Model/ReadSignupResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendValidateResponse**
> sendValidateResponse($body)

Validation Result

A signup session must be validated by your system before briqpay will complete the flow for the user. This is done by being notified by a backend call to the endpoint your provided in  ```merchanturls.signupwebhook``` when creating the session Below is how you should format the request if you are using the backend version of validation \\nOnce a customer has pressed  the button for complete registration in the iframe. Briqpay will post a notification to your servers as described above. In order to ensure a good UX briqpay will suspend the iframe for 7 seconds. If your service does not post back a successful validation within that time, a fail message will be displayed to the user. ![Signup validation](https://cdn.briqpay.com/static/developer-portal/signup-validation.png) \\nBefore proceeding with the registration of the account, you should also ensure that the response from briqpay is successful

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\SignupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\ValidateSignupResponse(); // \Briqpay\Models\ValidateSignupResponse | 

try {
    $apiInstance->sendValidateResponse($body);
} catch (Exception $e) {
    echo 'Exception when calling SignupApi->sendValidateResponse: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\ValidateSignupResponse**](../Model/ValidateSignupResponse.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **yoursignupwebhookPost**
> yoursignupwebhookPost($body)

SignupWebhook Request

This is the request from Briqpay to you on the url you defined un merchanturls.signupwebhook when creating the signup session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Briqpay\Api\SignupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Briqpay\Models\BriqpayValidateRequest(); // \Briqpay\Models\BriqpayValidateRequest | 

try {
    $apiInstance->yoursignupwebhookPost($body);
} catch (Exception $e) {
    echo 'Exception when calling SignupApi->yoursignupwebhookPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\BriqpayValidateRequest**](../Model/BriqpayValidateRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

