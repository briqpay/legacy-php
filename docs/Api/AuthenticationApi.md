# Briqpay\AuthenticationApi

All URIs are relative to *https://playground-api.briqpay.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAuthToken**](AuthenticationApi.md#getauthtoken) | **GET** /auth | Generate bearer token
[**getAuthTokenForSession**](AuthenticationApi.md#getauthtokenforsession) | **GET** /auth/{sessionid} | Generate a new auth token for session
[**getSignupSessionAuth**](AuthenticationApi.md#getsignupsessionauth) | **GET** /auth/signup/{sessionid} | Get a new auth token for a signup session

# **getAuthToken**
> \Briqpay\Models\AuthenticationResponse getAuthToken()

Generate bearer token

In order to create a checkout session you will need a Bearer token. Use your merchantID and secret provided by briqpay

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Briqpay\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Briqpay\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getAuthToken();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->getAuthToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Briqpay\Models\AuthenticationResponse**](../Model/AuthenticationResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAuthTokenForSession**
> \Briqpay\Models\AuthenticationResponse getAuthTokenForSession($sessionid)

Generate a new auth token for session

Get Bearer Token for assigned session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Briqpay\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Briqpay\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | Session id for the new bearer token

try {
    $result = $apiInstance->getAuthTokenForSession($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->getAuthTokenForSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| Session id for the new bearer token |

### Return type

[**\Briqpay\Models\AuthenticationResponse**](../Model/AuthenticationResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSignupSessionAuth**
> \Briqpay\Models\SignupAuthTokenResponse getSignupSessionAuth($sessionid)

Get a new auth token for a signup session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Briqpay\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Briqpay\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | Session id for the new bearer token

try {
    $result = $apiInstance->getSignupSessionAuth($sessionid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->getSignupSessionAuth: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| Session id for the new bearer token |

### Return type

[**\Briqpay\Models\SignupAuthTokenResponse**](../Model/SignupAuthTokenResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

