# Briqpay\CheckoutApi

All URIs are relative to *https://playground-api.briqpay.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelRecurringToken**](CheckoutApi.md#cancelrecurringtoken) | **POST** /checkout/v1/recurring/token/cancel | Cancel a recurring token
[**createSession**](CheckoutApi.md#createsession) | **POST** /checkout/v1/sessions | Create a payment session
[**patchCheckoutSession**](CheckoutApi.md#patchcheckoutsession) | **POST** /checkout/v1/sessions/patch | Patch a session
[**purchaseDecision**](CheckoutApi.md#purchasedecision) | **POST** /v2/session/{sessionid}/decision/purchase | Approve or deny a purchase a session
[**readRecurringToken**](CheckoutApi.md#readrecurringtoken) | **POST** /checkout/v1/recurring/token | Read a recurring token
[**readSession**](CheckoutApi.md#readsession) | **POST** /checkout/v1/readsession | Read session
[**updateSession**](CheckoutApi.md#updatesession) | **POST** /checkout/v1/sessions/update | Update a session
[**yourchargewebhookPost**](CheckoutApi.md#yourchargewebhookpost) | **POST** /yourchargewebhook | Charge result

# **cancelRecurringToken**
> cancelRecurringToken($body)

Cancel a recurring token

In order to cancel all future charges on this token, you need to cancel it

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\CancelRecurringTokenRequest(); // \Briqpay\Models\CancelRecurringTokenRequest | 

try {
    $apiInstance->cancelRecurringToken($body);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->cancelRecurringToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\CancelRecurringTokenRequest**](../Model/CancelRecurringTokenRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createSession**
> \Briqpay\Models\Session createSession($body)

Create a payment session

Once your customer has entered your checkout. You should create the payment session towards briqpay. The response will include the snippet needed to render our checkout iframe. If your customer is already logged into your systems you are able to provide briqpay with billing and shipping details at this point.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\SessionRequest(); // \Briqpay\Models\SessionRequest | Mandatory method to create a payment session

try {
    $result = $apiInstance->createSession($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->createSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\SessionRequest**](../Model/SessionRequest.md)| Mandatory method to create a payment session | [optional]

### Return type

[**\Briqpay\Models\Session**](../Model/Session.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **patchCheckoutSession**
> patchCheckoutSession($body)

Patch a session

Use this method if you only want to update references and urls of a session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\PatchSessionRequest(); // \Briqpay\Models\PatchSessionRequest | 

try {
    $apiInstance->patchCheckoutSession($body);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->patchCheckoutSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\PatchSessionRequest**](../Model/PatchSessionRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseDecision**
> purchaseDecision($sessionid, $body)

Approve or deny a purchase a session

This method can be used for a last minute validation of the order.  If needed, request this functionality from your Briqpay representative

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = "sessionid_example"; // string | Session to be decided on
$body = new \Briqpay\Models\PurchaseDecisionRequest(); // \Briqpay\Models\PurchaseDecisionRequest | 

try {
    $apiInstance->purchaseDecision($sessionid, $body);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->purchaseDecision: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| Session to be decided on |
 **body** | [**\Briqpay\Models\PurchaseDecisionRequest**](../Model/PurchaseDecisionRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **readRecurringToken**
> \Briqpay\Models\ReadRecurringToken readRecurringToken($body)

Read a recurring token

This method allows you to read out all data on a recurring token, including completed payments

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\ReadRecurringTokenRequest(); // \Briqpay\Models\ReadRecurringTokenRequest | 

try {
    $result = $apiInstance->readRecurringToken($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->readRecurringToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\ReadRecurringTokenRequest**](../Model/ReadRecurringTokenRequest.md)|  | [optional]

### Return type

[**\Briqpay\Models\ReadRecurringToken**](../Model/ReadRecurringToken.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **readSession**
> \Briqpay\Models\Session readSession($body)

Read session

After a completed purchase you can read the session in order to access the customer details.  At this point briqpay will provide you with company identification numbers as well as billing and shipping details.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\GetSessionRequest(); // \Briqpay\Models\GetSessionRequest | Read an existing session

try {
    $result = $apiInstance->readSession($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->readSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\GetSessionRequest**](../Model/GetSessionRequest.md)| Read an existing session | [optional]

### Return type

[**\Briqpay\Models\Session**](../Model/Session.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSession**
> updateSession($body)

Update a session

If you want to update an ongoing session you can post to the ``/update`` endpoint using the correct Bearer Token for the session you wish to update This is valuable if you offer the ability to change cart values of freight options on the checkout page.  All variables are required. If you want to remove values, you should send in empty strings. If there are values you do not want to update, you should send in its existing value

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Briqpay\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Briqpay\Models\UpdateSessionRequest(); // \Briqpay\Models\UpdateSessionRequest | 

try {
    $apiInstance->updateSession($body);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->updateSession: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\UpdateSessionRequest**](../Model/UpdateSessionRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **yourchargewebhookPost**
> yourchargewebhookPost($body)

Charge result

Briqpay will send a webhook for all succesfull, attempted, and failed recurring charges

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Briqpay\Models\RecurringChargeResultRequest(); // \Briqpay\Models\RecurringChargeResultRequest | 

try {
    $apiInstance->yourchargewebhookPost($body);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->yourchargewebhookPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Briqpay\Models\RecurringChargeResultRequest**](../Model/RecurringChargeResultRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

