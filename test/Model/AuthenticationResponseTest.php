<?php
/**
 * AuthenticationResponseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Briqpay
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * BriqPay Api
 *
 * This is the API documentation for Briqpay. You can find out more about us and our offering at our website [https://briqpay.com](https://briqpay.com) In order to get credentials to the playgrund API  Please register at [https://app.briqpay.com](https://app.briqpay.com) # Introduction  Briqpay Checkout is an inline checkout solution for your b2b ecommerce.  Briqpay Checkout gives you the flexibility of controlling your payment methods and credit rules while optimizing the UX for your customers # SDKs Briqpay offers standard SDKs to PHP and .NET based on these swagger definitions. You can download them respively or use our swagger defintitions to codegen your own versions. #### For .NET `` Install-Package Briqpay `` #### For PHP `` composer require briqpay/php-sdk  `` # Standard use-case As a first step of integration you will need to create a checkout session. \\n\\nIn this session you provide Briqpay with the basic information necessary. In the response from briqpay you will recieve a htmlsnippet that is to be inserted into your frontend. The snippet provided by briqpay will render an iframe where the user will complete the purchase. Once completed, briqpay will redirect the customer to a confirmation page that you have defined.    ![alt](https://cdn.briqpay.com/static/developer-portal/checkout-integration.png)  # JavaScript SDK The first step of integration is to add our JS to your site just before closing the  ``<head>`` tag. This ensures that our JS library is avaliable to load the checkout.  ``<script src=\"https://api.briqpay.com/briq.min.js\"></script>``  Briqpay offers a few methods avaliable through our Javascript SDK. The library is added by our iframe and is avalable on ``window._briqpay`` If you offer the posibility to update the cart or order amonts on the checkout page, the JS library will help you.  If your store charges the customer different costs and fees depening on their shipping location, you can listen to the ``addressupdate``event in order to re-calculate the total cost. ```javascript     window._briqpay.subscribe('addressupdate', function (data) {   console.log(data)     }) ```  If your frontend needs to perform an action whe the signup has completed, listen to the ``signup_finalized`` event. ```javascript     window._briqpay.subscribe('signup_finalized', function (status) {       // redirect or handle status 'success' / 'failure'     }) ```  If you allow customers to change the total cart value, you can utilise the JS library to suspend the iframe while you perform a backen update call towards our services. As described below: ![alt](https://cdn.briqpay.com/static/developer-portal/suspend-resume.png) The iframe will auto-resume after 7 seconds if you dont call ``_briqpay.resume()`` before  # Test Data In order to verify your integration you will neeed to use test data towards our credit engine.  ## Company identication numbers * 1111111111 - To recieve a high credit scoring company ( 100 in rating) * 2222222222 - To test the enviournment with a bad credit scoring company (10 in rating)  ## Card details In our playground setup your account is by default setup with a Stripe integration. In order to test out the card form you can use the below card numbers:  * 4000002500003155 - To mock 3ds authentication window * 4000000000000069 Charge is declined with an expired_card code.  You can use any valid expiry and CVC code  # Authentication  Briqpay utilizes JWT in order to authenticate calls to our platform.  Authentication tokens expire after 48 hours, and at that point you can generate a new token for the given resource using the ``/auth`` endpoint.  - Basic Auth - only used on the auth endpoint in order to get the Bearer Token  - JWT Bearer Token - All calls towards the API utlizes this method\"
 *
 * OpenAPI spec version: 1.0.0
 * Contact: hello@briqpay.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.34
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Briqpay;

/**
 * AuthenticationResponseTest Class Doc Comment
 *
 * @category    Class
 * @description AuthenticationResponse
 * @package     Briqpay
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AuthenticationResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AuthenticationResponse"
     */
    public function testAuthenticationResponse()
    {
    }

    /**
     * Test attribute "token"
     */
    public function testPropertyToken()
    {
    }
}
